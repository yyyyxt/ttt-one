package com.ttt.one.common.utils.constant;

/**
 * 外挂信息 常量
 */
public class InfoConstant {
    public static final String INFO_LIKED_USER_KEY = "INFO:LIKED:USER";
    /**
     * 评论点赞总数key
     * redis key命名规范推荐使用大写，单词与单词之间使用:
     */
    public static final String TOTAL_LIKE_COUNT_KEY ="TOTAL:LIKE:COUNT";
}
