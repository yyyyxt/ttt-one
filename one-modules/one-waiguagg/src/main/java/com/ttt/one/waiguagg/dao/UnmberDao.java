package com.ttt.one.waiguagg.dao;

import com.ttt.one.waiguagg.entity.UnmberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 外挂账号
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2021-08-09 10:17:14
 */
@Mapper
public interface UnmberDao extends BaseMapper<UnmberEntity> {
	
}
