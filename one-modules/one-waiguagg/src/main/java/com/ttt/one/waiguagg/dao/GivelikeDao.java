package com.ttt.one.waiguagg.dao;

import com.ttt.one.waiguagg.entity.GivelikeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 点赞表
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2021-11-22 16:21:39
 */
@Mapper
public interface GivelikeDao extends BaseMapper<GivelikeEntity> {
	
}
