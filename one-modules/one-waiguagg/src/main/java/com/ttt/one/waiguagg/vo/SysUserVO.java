package com.ttt.one.waiguagg.vo;

import lombok.Data;

@Data
public class SysUserVO {
    private String username;
}
