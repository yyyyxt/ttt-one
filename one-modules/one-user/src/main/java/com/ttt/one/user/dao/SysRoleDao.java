package com.ttt.one.user.dao;

import com.ttt.one.user.entity.SysRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色表
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2022-05-02 19:24:18
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	
}
