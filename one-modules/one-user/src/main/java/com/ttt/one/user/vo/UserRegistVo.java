package com.ttt.one.user.vo;

import lombok.Data;

/**
 * 用户注册 vo
 */
@Data
public class UserRegistVo {
    private String username;
    private String password;
    private String phone;

}
