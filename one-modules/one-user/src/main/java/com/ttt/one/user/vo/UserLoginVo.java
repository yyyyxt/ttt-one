package com.ttt.one.user.vo;

import lombok.Data;

/**
 * 用户登录 vo
 */
@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
