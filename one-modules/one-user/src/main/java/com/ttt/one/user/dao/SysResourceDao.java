package com.ttt.one.user.dao;

import com.ttt.one.user.entity.SysResourceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 资源表
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2022-05-02 19:24:18
 */
@Mapper
public interface SysResourceDao extends BaseMapper<SysResourceEntity> {
	
}
