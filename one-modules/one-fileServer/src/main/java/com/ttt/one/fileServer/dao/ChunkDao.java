package com.ttt.one.fileServer.dao;

import com.ttt.one.fileServer.entity.ChunkEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2021-08-13 17:41:45
 */
@Mapper
public interface ChunkDao extends BaseMapper<ChunkEntity> {
	
}
