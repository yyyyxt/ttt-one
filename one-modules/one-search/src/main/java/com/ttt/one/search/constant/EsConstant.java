package com.ttt.one.search.constant;

public class EsConstant {
    public static final String WAIGUA_INDEX = "waiguas"; //数据在ES的索引
    public static final Integer WAIGUA_PAGESIZE = 22; //数据在ES的分页 每页几个
    public static final String ONE_WAIGUAGG_INDEX = "one-waiguagg"; //数据在ES的索引
    public static final String ONE_AUTH_SERVER_INDEX = "one-auth-server"; //数据在ES的索引

}
