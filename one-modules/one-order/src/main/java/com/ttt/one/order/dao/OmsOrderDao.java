package com.ttt.one.order.dao;

import com.ttt.one.order.entity.OmsOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2022-01-25 14:14:54
 */
@Mapper
public interface OmsOrderDao extends BaseMapper<OmsOrderEntity> {
	
}
