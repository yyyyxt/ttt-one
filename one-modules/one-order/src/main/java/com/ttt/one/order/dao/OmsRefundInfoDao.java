package com.ttt.one.order.dao;

import com.ttt.one.order.entity.OmsRefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author ttt
 * @email 496427196@qq.com
 * @date 2022-01-25 14:14:54
 */
@Mapper
public interface OmsRefundInfoDao extends BaseMapper<OmsRefundInfoEntity> {
	
}
